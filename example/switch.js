/**
 * --------- ПРИМЕРЫ ---------- *
 *            SWITCH
 * ---------------------------- *
 */
// проверяем что лежит в x
// возможные значения в case
switch (x) {
    case 'value1':  // if (x === 'value1')
        console.log('value1');
        // выполнение инструкций
        break;

    case 'value2':  // if (x === 'value2')
        console.log('value2');
        // выполнение инструкций
        break;

    // если не выполняется ни одно условие
    default:
        console.log('value1');
        // выполнение инструкций
        break;
}

// проверяем название нашего браузера
switch (browser) {
    case 'Chrome':
        console.log("Вы используйте Chrome!");
        break;

    // так мы группировали case
    // для каждого из группы будет выполняться одно и тоже   
    case 'Firefox':
    case 'Safari':
    case 'Opera':
        console.log('Эти браузеры мы тоже поддерживаем');
        break;

    default:
        console.log('Ваш браузер не поддерживается!');
}
