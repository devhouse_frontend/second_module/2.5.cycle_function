/**
 * ------------- ПРИМЕР ------------------ *
 *  УСЛОВНЫЕ ОПЕРАТОРЫ, SWITCH И ФУНКЦИИ
 * --------------------------------------- *
 *
 * Расчет возрастной категории по возрасту
 * @param age
 * @returns {string} возрастная категория
 */
function checkAge(age) {
    // Инициализируем начало сообщения
    let message = 'Это '

    // Диапазоны категорий
    if (age < 14) {
        message += 'ребенок';
    } else if (age < 18) {
        message += 'подросток';
    } else if (age < 75) {
        message += 'взрослый';
    } else {
        message += 'пожилой человек';
    }

    // Вырожденные случаи
    switch (age) {
        // сразу несколько вариантов
        case 0:
        case 1:
        case 2:
            message += ' новорожденный';
            break;
        case 15:
            message += ' и недавно получил паспорт';
    }

    return message;
}

// Выводим то что возвращает функция, для разных возрастов
console.log(checkAge(2));
console.log(checkAge(3));
console.log(checkAge(15));
console.log(checkAge(19));
console.log(checkAge(80));
